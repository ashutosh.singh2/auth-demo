import React, { Component } from "react";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import GoogleLogin from "react-google-login";
import { GoogleLogout } from "react-google-login";
import "./App.css";
import { access } from "fs";
class App extends Component {
  apiCall = async () => {
    fetch("https://developers.zomato.com/api/v2.1/categories", {
      method: "GET",
      headers: {
        "user-key": "f9597a254c40e28de9289c991c4fe192",
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(posts => console.log(posts));
  };

  responseFacebook = response => {
    this.setState({ response: response });
    console.log(response);
  };
  logout = response => {
    console.log(response);
  };
  responseGoogle = response => {
    console.log(response);
  };
  render() {
    return (
      <div className="App">
        {/* ---------------------------------google login------------------------------------- */}
        {/* <GoogleLogin
          clientId="1089526938509-okrvm3ccq39hhqar1031oer6ca787uqt.apps.googleusercontent.com"
          buttonText="Login"
          onSuccess={responseGoogle}
          onFailure={responseGoogle}
          cookiePolicy={"single_host_origin"}
        /> */}

        <GoogleLogin
          clientId="1089526938509-okrvm3ccq39hhqar1031oer6ca787uqt.apps.googleusercontent.com"
          render={renderProps => (
            <button
              onClick={renderProps.onClick}
              disabled={renderProps.disabled}
            >
              Google button
            </button>
          )}
          buttonText="Login"
          onSuccess={this.responseGoogle}
          onFailure={this.responseGoogle}
          cookiePolicy={"single_host_origin"}
        />
        <GoogleLogout buttonText="Logout" onLogoutSuccess={this.logout} />
        {/* ------------- ---------------------facebook------------------------------------------------------------ */}
        <FacebookLogin
          appId="2479143425696885"
          callback={this.responseFacebook}
          render={renderProps => (
            <button onClick={renderProps.onClick}>fb</button>
          )}
        />
        <button
          onClick={e => {
            e.preventDefault();
            window.FB.logout(this.responseFacebook);
          }}
        >
          logout
        </button>
        {/* ---------------------------------api call of zomat----------------------------------------------------- */}
        <header className="App-header">
          <button onClick={this.apiCall}>delete</button>
        </header>
      </div>
    );
  }
}

export default App;
